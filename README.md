# [Payday 2] Infamy Card Priority

Changes the priority order to show infamy cards over weapon skins in a lobby. 

_Note: Only shows for the modded client._

## Requirements
[BLT Hook](https://superblt.znix.xyz/)

## Installation
1. Download the [latest version](https://gitlab.com/kaninte/infamy-card-priority/-/releases).
2. Unzip the file.
3. Copy the Infamy Card Priority folder into your mod folder.
