Hooks:PostHook(BaseNetworkSession, "create_local_peer", "set_local_peer_rank", function (self)
	local rank = managers.experience:current_rank()
	self._local_peer:set_rank(rank)
end)